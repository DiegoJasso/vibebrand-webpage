var nodeStatic = require('node-static'),
    util = require('util'),
    http = require('http');
var webroot = './Frontend',
    port = process.env.PORT || 1337;
var file = new(nodeStatic.Server)(webroot, {
    cache: 600,
    eaders: {'X-Powered-By': 'node-static'}
});
function start() {
    var handlers = function (req, res) {
        req.addListener('end', function(){
            file.serve(req, res, function(err, result){
                if (err){
                    console.error('Error serving %s - %s', req.url, err.message);
                    if (err.status == 404 || err.status == 500) {
                        file.serveFile(util.format('/error-pages/%d.html', err.status), err.status, {}, req, res);
                    }
                    else {
                        res.writeHead(err.status, err.headers);
                        res.end();
                    }
                }
                else {
                    console.log('%s - %s', req.url, res.message);
                }
            });
        });
    };
    http.createServer(handlers).listen(port);
    console.log('Server runningat http://vibebrand.herokuapp.com');
}

exports.start = start;
